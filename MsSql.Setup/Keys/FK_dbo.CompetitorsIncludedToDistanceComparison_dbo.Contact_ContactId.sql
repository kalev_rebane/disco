﻿ALTER TABLE [dbo].[tblCompetitorsIncludedToDistanceComparison]  WITH CHECK ADD  CONSTRAINT [FK_dbo.CompetitorsIncludedToDistanceComparison_dbo.Contact_ContactId] FOREIGN KEY([ContactId])
REFERENCES [dbo].[tblContact] ([ContactId])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[tblCompetitorsIncludedToDistanceComparison] CHECK CONSTRAINT [FK_dbo.CompetitorsIncludedToDistanceComparison_dbo.Contact_ContactId]
GO
