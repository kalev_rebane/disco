﻿ALTER TABLE [dbo].[tblComparisonReports]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ComparisonReports_dbo.DistanceComparison_DistanceComparisonId] FOREIGN KEY([DistanceComparisonId])
REFERENCES [dbo].[tblComparisonReports] ([DistanceComparisonId])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[tblComparisonReports] CHECK CONSTRAINT [FK_dbo.ComparisonReports_dbo.DistanceComparison_DistanceComparisonId]
GO

