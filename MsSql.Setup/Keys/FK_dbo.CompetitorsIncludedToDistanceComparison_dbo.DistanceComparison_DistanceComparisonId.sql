﻿

ALTER TABLE [dbo].[tblCompetitorsIncludedToDistanceComparison]  WITH CHECK ADD  CONSTRAINT [FK_dbo.CompetitorsIncludedToDistanceComparison_dbo.DistanceComparison_DistanceComparisonId] FOREIGN KEY([DistanceComparisonId])
REFERENCES [dbo].[tblDistanceComparison] ([DistanceComparisonId])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[tblCompetitorsIncludedToDistanceComparison] CHECK CONSTRAINT [FK_dbo.CompetitorsIncludedToDistanceComparison_dbo.DistanceComparison_DistanceComparisonId]
GO

