﻿ALTER TABLE [dbo].[tblDistanceComparison]  WITH CHECK ADD  CONSTRAINT [FK_dbo.DistanceComparison_dbo.Contact_CustomerId] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[tblContact] ([ContactId])
GO

ALTER TABLE [dbo].[tblDistanceComparison] CHECK CONSTRAINT [FK_dbo.DistanceComparison_dbo.Contact_CustomerId]
GO
