﻿

ALTER TABLE [dbo].[tblAddress]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Address_dbo.Contact_ContactId] FOREIGN KEY([ContactId])
REFERENCES [dbo].[tblContact] ([ContactId])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[tblAddress] CHECK CONSTRAINT [FK_dbo.Address_dbo.Contact_ContactId]
GO

