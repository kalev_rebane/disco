﻿
CREATE TABLE [dbo].[tblCompetitorsIncludedToDistanceComparison](
	[DistanceComparisonId] [int] NOT NULL,
	[ContactId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.CompetitorsIncludedToDistanceComparison] PRIMARY KEY CLUSTERED 
(
	[DistanceComparisonId] ASC,
	[ContactId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
) 

GO
