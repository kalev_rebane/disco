﻿
CREATE TABLE [dbo].[tblDistanceComparison](
	[DistanceComparisonId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.DistanceComparison] PRIMARY KEY CLUSTERED 
(
	[DistanceComparisonId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

