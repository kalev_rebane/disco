﻿

CREATE TABLE [dbo].[tblAddress](
	[AddressId] [int] IDENTITY(1,1) NOT NULL,
	[PostCode] [nvarchar](18) NOT NULL,
	[CountryCodeIso3] [nvarchar](3) NULL,
	[Line1] [nvarchar](max) NULL,
	[Phone] [nvarchar](33) NULL,
	[Email] [nvarchar](66) NULL,
	[Lat] [float] NULL,
	[Lng] [float] NULL,
	[ContactId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Address] PRIMARY KEY CLUSTERED 
(
	[AddressId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
) 

GO