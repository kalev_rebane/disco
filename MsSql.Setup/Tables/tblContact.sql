﻿

CREATE TABLE [dbo].[tblContact](
	[ContactId] [int] IDENTITY(1,1) NOT NULL,
	[ContactType] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Phone] [nvarchar](33) NULL,
	[Email] [nvarchar](66) NULL,
	[AddressesModified] [datetime] NULL,
	[HiresPerYear] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Contact] PRIMARY KEY CLUSTERED 
(
	[ContactId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
) 

GO
