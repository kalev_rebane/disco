﻿
CREATE TABLE [dbo].[tblComparisonReports](
	[ComparisonReportId] [int] IDENTITY(1,1) NOT NULL,
	[Serialized_Contact_Customer] [varbinary](max) NULL,
	[Serialized_ICollectionFetcherLogForTarget] [varbinary](max) NULL,
	[Created] [datetime] NOT NULL,
	[DistanceComparisonId] [int] NOT NULL,
	[ProcessedElements] [int] NOT NULL,
	[NoReplyError] [varchar](100) NULL,
 CONSTRAINT [PK_dbo.ComparisonReports] PRIMARY KEY CLUSTERED 
(
	[ComparisonReportId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
